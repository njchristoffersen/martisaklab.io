---
layout: null
permalink: /.well-known/keybase.txt
---

I hereby claim:

  * I am an admin of https://blog.martisak.se
  * I am martisak (https://keybase.io/martisak) on keybase.
  * I have a public key ASBz8skAt1jLQK9cBerzLdypJlEiMlmMfo0983lBuzV9ewo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0101e62a1bdf127756213339205b409e81f229b84a08bd19fd18a561fea5542619ed0a",
      "host": "keybase.io",
      "kid": "012073f2c900b758cb40af5c05eaf32ddca926512232598c7e8d3df37941bb357d7b0a",
      "uid": "705d7355a220fd52c88065cd46fd8519",
      "username": "martisak"
    },
    "merkle_root": {
      "ctime": 1552142713,
      "hash": "af1aafb4456f389fcf220edd186c646e0f734fb30031940fcfe01c86d08a8aefbef24ee8874726fb0d1fd2a6ac106167a2f33aef462d2a4a42a4933133fb2616",
      "hash_meta": "c26bfd3f8f13bc5fba5b5f1ef90b4626144ab3838245413fcc9b1ded62f1e2e3",
      "seqno": 4908982
    },
    "service": {
      "entropy": "WPKO/VNnF6y9rMAaB1pg70hx",
      "hostname": "blog.martisak.se",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "3.1.0"
  },
  "ctime": 1552142727,
  "expire_in": 504576000,
  "prev": "639dba1a1c220691a6cd0f51c7f8fc79defceb87cf8c091d162d7896b9579762",
  "seqno": 19,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgc/LJALdYy0CvXAXq8y3cqSZRIjJZjH6NPfN5Qbs1fXsKp3BheWxvYWTESpcCE8QgY526GhwiBpGmzQ9Rx/j8ed7864fPjAkdFi14lrlXl2LEIGeeG9/k4M2YRYm90kQHM73JQJgcncvA/KnCJlqSV9iIAgHCo3NpZ8RA9qm2K53yyOEO+FNs6eYjrPPXwszXsG/m0/jAymQaKcMUkjjJ5qyraFVjefeZqCOgHKpSz0017uwFpILF0pvUAqhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIKw7tg4Wz8x3mj4w5RS4Z+qRIUezfdWiDqgaZyseK7fno3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/martisak