import pandas as pd
import numpy as np
import seaborn as sns
import os


if __name__ == "__main__":
    iris = sns.load_dataset('iris')

    df = (iris
        .groupby("species")
        .mean()
        .reset_index()
        )

    with open(os.path.splitext(os.path.basename(__file__))[0] + ".tbl", "w") as f:
        f.write(df.head().to_latex())
