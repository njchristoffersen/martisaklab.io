---
title:  Adding Sparklines to LaTeX tables using Pandas
categories: reproducibility academia visualization
layout: "post"
tags:
  - visualization
  - tables
  - example
image:
  path: /images/blog-2.jpg
description: Learn how to make your tables stand out even more with Sparklines.
---

**Tables in scientific papers often look less than professional, and sometimes this can even get in the way of understanding the message. In this blog post we will learn how to add sparklines to a LaTeX table, which not only makes your table stand out, but also allows for conveying information about for example trends in time-series.**

<!--more-->

## Introduction

In an [earlier post]({% post_url 2021-04-10-publication_ready_tables %}), we looked at using Pandas {% cite reback2020pandas mckinney-proc-scipy-2010 %} to produce nice looking tables with no manual steps. In this post we will take it one step further by adding sparklines {% cite Tufte:1986:VDQ:33404 EdwardTu38:online EdwardTu79:online  Bissantz57:online %} to our table using 
the `sparklines` {% cite sparklines2017 %} LaTeX package.

A sparkline is a very small chart, often in a text or in a table without axis or coordinates, that presents some measurement in an "intense, simple, wordlike graphics". For example, The [Dow Jones Industrial Average for February 7, 2006](https://commons.wikimedia.org/wiki/File:Sparkline_dowjones_new.svg)&nbsp;<img src="/images/dowjones.png" style="height: 1em;"/>&nbsp;(Licensed under [CC BY-SA 2.5](https://creativecommons.org/licenses/by-sa/2.5/)).

{% include figure.html url="/images/table-sparklines.png" description="Example table using the Iris dataset from the `seaborn` library." colclass="col-md-6" %}
 
Sparklines are useful to show trends, highlight important events in time-series etc, which are otherwise hard to convey to a reader. They are especially useful
when there are many such time-series and a regular figure would take up too much valuable space. In this post we will add them to a LaTeX table, but they can be used in running text, in spreadsheets and in many other situations.

## The code
### The Python code

First we will need some data - here we use [weather data](https://raw.githubusercontent.com/plotly/datasets/master/2016-weather-data-seattle.csv) from [Seattle](https://www.seattle.gov/), a Plotly {% cite plotly %} dataset. We will take this opportunity to clean it a little bit.

{% highlight python linenos %}
# First download the data from plotly's GitHub repository
df = pd.read_csv(
    'https://raw.githubusercontent.com/plotly/datasets/master/2016-weather-data-seattle.csv')

df['month'] = pd.to_datetime(df['Date']).dt.month

# we define a dictionary with months that we'll use later
month_dict = {1: 'January', 2: 'February',
              3: 'March', 4: 'April',
              5: 'May', 6: 'June',
              7: 'July', 8: 'August',
              9: 'September', 10: 'October',
              11: 'November', 12: 'December'}

df = df.sort_values("month")
df["datetime"] = pd.to_datetime(df.Date)
df = df.drop(["Date"], axis=1)
df = df.dropna()
{% endhighlight %}

Now we would like to group this data per month. Thereafter, we apply a magic function `f`. 

{% highlight python linenos %}
df4 = (df
       .groupby("month")
       .apply(f).reset_index()
       )
{% endhighlight %}

The function `f` will calculate the mean, standard deviation and those important, but frankly boring, metrics. More interestingly it do one more thing - it will render a sparkline {% cite Tufte:1986:VDQ:33404 EdwardTu38:online EdwardTu79:online  Bissantz57:online %}.

{% highlight python linenos %}
def f(x):
    d = {}

    d['max'] = x['Mean_TemperatureC'].max()
    d['mean'] = x['Mean_TemperatureC'].mean()
    d['std'] = x['Mean_TemperatureC'].std()
    d['min'] = x['Mean_TemperatureC'].min()
    d['sparkline'] = sparkline(x)
    return pd.Series(d, index=['mean', 'std', 'min', 'max', 'sparkline'])
{% endhighlight %}

Rendering the sparkline consists of writing a `sparkline` environment to file, so we won't cover it here. See [the full Python code here](https://gist.github.com/martisak/a7388765f0ece78457a07a0f91cdb1db) and the `sparklines` {% cite sparklines2017 %} [LaTeX package documentation](http://mirrors.ctan.org/graphics/sparklines/sparklines.pdf).

### The LaTeX Code

Now that we have generated our table using Pandas, we need to include it in 
our document. See [table.tex](https://gist.github.com/martisak/a3575c4891f5ad08a493871077a5413e),  where the included `includes/macros.tex` contains some libraries and macros that we need, see  [macros.tex](https://gist.github.com/martisak/ff08cabcc70f58c5ae1a612a21f93f8d).

Most importantly, we load the `sparklines` LaTeX package {% cite sparklines2017 %}. We also need to define a new command for a rectangle that is defined by its left and right values. 

{% highlight python linenos %}
{% raw %}
\def\sparkrectangleh #1 #2 {%
   \ifdim #1pt > #2pt
        \errmessage{The left corner #1 of rectangle cannot be lower than #2}%
   \fi
   {\pgfmoveto{\pgforigin}\color{sparkrectanglecolor}%
   \pgfrect[fill]{\pgfxy(#1, 0)}{\pgfxy(#2-#1,1)}}}%
{% endraw %}
{% endhighlight %}

## Related work

spark {% cite blevins2013 %} is a LaTeX package for generating sparklines.
ltxsparklines: Lightweight Sparklines for a LaTeX Document {% cite ltxspark57:online %} is an interface for R to `sparklines`{% cite sparklines2017 %}.

## Conclusion

Adding a table to your paper is a good idea. It is also a good idea to invest some time into making this table easy to read instead of just presenting a wall of numbers. In an [earlier post]({% post_url 2021-04-10-publication_ready_tables %}), we looked at using Pandas {% cite reback2020pandas mckinney-proc-scipy-2010 %} to produce nice looking tables where we highlighted some summary statistics. In this post we took this one step further by adding sparklines {% cite Tufte:1986:VDQ:33404 EdwardTu38:online EdwardTu79:online  Bissantz57:online %}.

Easily digested tables makes it easier to understand the idea and the message we are trying 
to convey. In fact there is some
evidence&#160;{% cite Huang2018 %} that the visual appearance of a paper is
important and that improving the paper gestalt reduces risk of getting a paper
rejected. In order to convey an idea efficiently we need to remove barriers so that the reader can understand this idea with as little cognitive effort as possible, and hopefully we have presented one way of achieving this here. We leave it to the reader to integrate this method into a Python package that is easy to use.
