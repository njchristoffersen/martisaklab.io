---
title: Top 10 LaTeX packages for academic writing
layout: "post"
image:
  path:  /images/blog.jpg
categories:
    - academia
    - latex
tags:
    - latex
    - packages
---

**In academic writing with LaTeX there are a lot of things that can be frustrating to the author. For many of these things there exists many packages that can help alleviate this frustration, but it is hard to find them. In this post I  list 10 of my favorite packages to help remove some of this frustation, and make your papers look nicer so that you have a higher probability of getting your paper accepted. Hopefully.**

<!--more-->

## Introduction

At the time of writing, [The Comprehensive TEX Archive Network (CTAN)](https://www.ctan.org/) has 5841 packages. Many of these are used very often, such as `amsmath`, `graphics` and `xcolor` {% cite Cormode2012 %}, and some are not used enough. This post is a list of packages that I find very useful in academic writing, but yet I don't see them being used enough. And a blog isn't really a blog without a top ten list, I guess.

Without further ado, in alphabetical order, here are 10 useful LaTeX packages for your writing pleasure.

## Packages

### adjustbox

The `adjustbox` package {% cite Scharrer2012 %} allows scaling of a figure or plot, for example one made in TikZ.

{% highlight tex linenos %}
\usepackage{adjustbox}                % Balances the columns of the last page
{% endhighlight %}

Here is an example from a real paper, slightly edited for clarity.

{% highlight tex linenos %}
\begin{figure}[t!]
    \centering
    \begin{adjustbox}{width=.75\linewidth}%
        \input{./figures/my_tikz_figure.tex}%
    \end{adjustbox}%
    \caption{An overview of our solution.}
    \label{fig:overview}
\end{figure}
{% endhighlight %}


### balance or flushend

The two columns on the last page of a two-column paper should be the same length. This package saves you from having to balance the columns manually {% cite Daly1999 %}.

{% highlight tex linenos %}
\usepackage{balance}                % Balances the columns of the last page
{% endhighlight %}

Simply add `\balance` in the first column of the last page, and you're done! Some users like the `flushend` package better as this doesn't require adding anything on the last page.

<div class="container figure">
<div class="row">
    <div class="col-md-4">
        <div class="papers"><img src="/assets/images/nothing.png" alt="Without balance" class="img-rounded" style="border: 1px solid gray;"/></div>
        <div class="caption">No column balancing.</div>
    </div>

    <div class="col-md-4">
        <div class="papers"><img src="/assets/images/balance.png" alt="With flushend" class="img-rounded" style="border: 1px solid gray;" /></div>
        <div class="caption">With <code>balance</code>.</div>
    </div>

    <div class="col-md-4">
        <div class="papers"><img src="/assets/images/flushend.png" alt="With flushend" class="img-rounded" style="border: 1px solid gray;" /></div>
        <div class="caption">With <code>flushend</code>.</div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="caption">Examples with and without column balancing using the <a href="https://mlsys.org/Conferences/2019/sysml2019style.tar.gz">2019 SysML LaTeX template example</a>.</div>
    </div>
</div>
</div>

### booktabs

Simply put, `booktabs` {% cite Fear2020 %} makes your tables look good!

Consider this example, adapted from [Animal table with booktabs.png](https://bit.ly/2UIkWyV) (licensed under the [Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0/deed.en) license).

{% highlight tex linenos %}
\usepackage{booktabs}              % Nicer tables
{% endhighlight %}

{% include figure.html url="/assets/images/booktabs.png" description="Booktabs example." %}

{% highlight tex linenos %}
\documentclass[10pt, a4paper]{article}
\usepackage{booktabs}
\usepackage[tableposition=top]{caption}

\begin{document}

\begin{table}
\centering
\caption{This is my table, there are many like it, but this one is mine.}
\label{tbl:mytable}
\begin{tabular}{llr}
\toprule
\multicolumn{2}{c}{Item} \\
\cmidrule(r){1-2}
Animal & Description & Price (\$) \\
\midrule
Gnat  & per gram & 13.65 \\
      & each     &  0.01 \\
Gnu   & stuffed  & 92.50 \\
Emu   & stuffed  & 33.33 \\
Armadillo & frozen & 8.99 \\
\bottomrule
\end{tabular}
\end{table}
\end{document}
{% endhighlight %}

### cleveref

Using the `cleveref` package {% cite Cubitt2012 %}, we can refer to figure, tables etc without specificing the type.
`See~\cref{tbl:mytable}` produces "See table 1".

{% highlight tex linenos %}
\usepackage[capitalise]{cleveref}  % Clever references to stuff.
{% endhighlight %}

The result is configurable, and we can for example change the figure type name with `\crefname{figure}{Fig.}{Fig.}`, where the first `Fig.` is the singular form and the second is the plural form.

### glossaries-extra

Using the package `glossaries-extra` we can add a list of terms and a list of acronyms to our document {% cite Talbot2016 Talbot2018 %}. It is also very useful for making sure that acronyms are written out the first time they are used.

{% highlight tex linenos %}
\usepackage[acronym,toc,symbols]{glossaries-extra}
\usepackage{glossary-longbooktabs}
\setabbreviationstyle[acronym]{long-short}

\makeglossaries
\newacronym{gcd}{GCD}{Greatest Common Divisor}

\newglossaryentry{eta}{name={\ensuremath{\eta}}, sort={eta}, description={Learning rate}, type={symbols}}

\clearpage
\printglossary[type=symbols,style=long-booktabs]
\clearpage
\printglossary[type=acronym,style=long-booktabs]
{% endhighlight %}

We can then use `\gls{gcd}` whenever we need to use the acronym Greatest Common Divisor (GCD) or `\gls{eta}` when we need to use the symbol for the learning rate. See [Glossaries on Overleaf](https://www.overleaf.com/learn/latex/glossaries) for more examples.


### microtype

Text typeset in LaTeX already looks great, but can be made even nicer with `microtype` {% cite Schlicht2019 %}. Looking deeper at the micro-typographics extensions provided by `microtype` is a great way to ensure getting trapped in a rabbit hole for a long time. Here are the settings I use, borrowed from the excellent post [Tips on Writing a Thesis in LaTeX](http://www.khirevich.com/latex/microtype/) {% cite Khirevich %}.

{% highlight tex linenos %}
\usepackage[
    protrusion=true,
    activate={true,nocompatibility},
    final,
    tracking=true,
    kerning=true,
    spacing=true,
    factor=1100]{microtype}
\SetTracking{encoding={*}, shape=sc}{40}
{% endhighlight %}

<div class="container figure">
<div class="row justify-content-md-center">
    <div class="col-md-4">
        <div class="papers"><img src="/assets/images/withoutmicrotype-0.png" alt="Without microtype" class="img-rounded" style="border: 1px solid gray;"/></div>
        <div class="caption">Without <code>microtype</code>.</div>
    </div>
    <div class="col-md-4">
        <div class="papers"><img src="/assets/images/withmicrotype-0.png" alt="With microtype" class="img-rounded" style="border: 1px solid gray;" /></div>
        <div class="caption">With <code>microtype</code>.</div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="caption">Examples with and without <code>microtype</code>. Examples were compiled with pdfLaTeX in TeX Live 2019.</div>
    </div>
</div>
</div>

### pgfplots

The `pgfplots` package {% cite Feuersanger2014 %} can be used for creating 2D and 3D plots. It is built on top of the PGF/TikZ package {% cite Tikz2014 %} and allows the creation of high-quality, customizable, complex graphs, charts, or diagrams directly within your document.

The package supports a wide variety of plot types, such as line plots, scatter plots, bar plots, histograms, and contour plots. `pgfplots` can load data from external files, enabling generation of plots without leaving the LaTeX environment. Used together with [`tikzplotlib`](https://pypi.org/project/tikzplotlib/) you can convert plots from `matplotlib`.

{% highlight tex linenos %}
\usepackage{pgfplots}
{% endhighlight %}

{% include figure.html url="/images/pgfplot.png" description="Example figure produced with this method." %}

See [Publication ready figures]({% post_url 2019-09-29-publication_ready_figures %}) for a longer post on the pgfplots package {% cite Feuersanger2014 %}.

### siunitx

Typesetting numbers with units is actually not trivial. `siunitx` {% cite Wright2009 %} adds, among others, the commands `\SI` and `\num` so that we can write `\SI{1024}{\byte}` or <code>\num&#123;&#123;&#125;e5&#125;</code>, which makes things look much nicer.

{% highlight tex linenos %}
\usepackage{siunitx}               % A better way of writing things with units
\sisetup{load-configurations = abbreviations, binary-units = true}
\DeclareSIUnit\px{px}
{% endhighlight %}

Another use for this package is for aligning numbers in a table, and we can directly use the table format `S` or `S[table-format=3.2]`, see also [Publication ready tables]({% post_url 2021-04-10-publication_ready_tables %}).

### tikz

TikZ and PGF {% cite Tikz2014 %} are packages for programatically creating figures, and deserve a longer writeup. 

{% highlight tex linenos %}
\usepackage{tikz}

\usetikzlibrary{shapes, decorations, calc, arrows}
\usetikzlibrary{3d,fit,backgrounds, decorations.text}
\usetikzlibrary{positioning, shapes.symbols}
\usetikzlibrary{decorations.pathreplacing, calligraphy}

\tikzset{>=latex}
{% endhighlight %}

{% include figure.html url="/assets/images/masks_standalone.svg" description="TikZ figure example from a recent paper." %}

See more examples of TikZ figures on [TeXample.net](http://www.texample.net/).

### savetrees

Here is a controversial package. It really doesn't belong on the recommended package list, but I will keep it here anyways. `savetrees` {% cite Pakin2007 %} is magic. It is highly useful when you are approaching a conference deadline and you have one page over the conference limit. Using the `subtle` mode, we can preserve the document layout and try to make LaTeX pack text tighter.

{% highlight tex linenos %}
\usepackage[subtle]{savetrees}
{% endhighlight %}

<div class="container figure">
<div class="row justify-content-md-center">
    <div class="col-md-3">
        <div class="papers"><img src="/assets/images/withoutsavetrees.png" alt="Without microtype" class="img-rounded" style="border: 1px solid gray;"/></div>
        <div class="caption">Without <code>savetrees</code>.</div>
    </div>
    <div class="col-md-3">
        <div class="papers"><img src="/assets/images/subtlesavetrees.png" alt="With microtype" class="img-rounded" style="border: 1px solid gray;" /></div>
        <div class="caption">With subtle <code>savetrees</code>.</div>
    </div>
    <div class="col-md-3">
        <div class="papers"><img src="/assets/images/moderatesavetrees.png" alt="Without microtype" class="img-rounded" style="border: 1px solid gray;"/></div>
        <div class="caption">With moderate <code>savetrees</code>.</div>
    </div>
    <div class="col-md-3">
        <div class="papers"><img src="/assets/images/extremesavetrees.png" alt="With microtype" class="img-rounded" style="border: 1px solid gray;" /></div>
        <div class="caption">With extreme <code>savetrees</code>.</div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="caption">Examples with various degrees of <code>savetrees</code>. The examples were compiled using pdfLaTeX from TeX Live 2019.</div>
    </div>
</div>
</div>

## Conclusion

There are probably more top ten lists of LaTeX packages than there are packages, and here's another one. Hope you enjoyed it!

What are your favorite packages? Comment below!
