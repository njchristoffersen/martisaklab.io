---
layout: post
title: Bootstrapping your next LaTeX project
image:
  path:  /images/blog-5.jpg
hidden: false
published: true
showrevisions: true
categories:
  - reproducibility
  - academia
tags:
  - latex
  - gitlab
  - continuous integration
description: Create a new LaTeX project with one simple command.
---

**The process of setting up a new LaTeX project is made up of many manual steps, resulting in a patchwork that already from the start is not _exercisable_ nor _complete_. In this post we will see how we can construct a solid starting point with a single command. This is part of a series to create the perfect open science <code>git</code> repository.**

<!--more-->

## Introduction

Setting up a new LaTeX project is usually a boring process where I do manual repetitive steps such as creating a directory structure, add file stubs, copy the <code>Makefile</code> from some old project, and more.

I like to use `git` for version control of LaTeX documents, so setting up a `git` repository is an important step. I do this even if I am working alone on a project. Version control allows me to track my progress, have a backup, and make sure the document is completely _reproducible_ from raw data. The principle of
_Reproducible Research_ {% cite buckheit1995wavelab claerbout1992electronic
Artifact18:online %} is to make data and computer code available for others to
analyze and criticize.

A good open source repository is exercisable and complete {% cite Monperrus2018 Artifact18:online %}. This means that it must be possible to fully reproduce the document, down to the last pixel, from running a single script in the repository. In [How to annoy your co-authors: a Gitlab CI pipeline for LaTeX]({% post_url 2020-05-11-gitlab-ci-latex-pipeline %}) we took a look at this can be done - but there were many manual steps which we will automate in this post.

Here are a few things I always do at the start of a new LaTeX project.

- Create a directory structure (for chapters, figures, data, bibliography, ...),
- Add a <code>.gitignore</code> default for LaTeX;
- Create a <code>main.tex</code>-file from a LaTeX template, usually from a conference template;
- Populate said file with author, working title;
- Initialize <code>git</code> locally;
- Create remote git repository on [Gitlab](https://www.gitlab.com), or a Gitlab CE instance;
- Add a <code>Makefile</code>, configure it to use the compiler that the template dictates;
- Add a <code>.gitlab-ci.yml</code> (see [How to annoy your co-authors: a Gitlab CI pipeline for LaTeX]({% post_url 2020-05-11-gitlab-ci-latex-pipeline %}));
- Create a Sublime project;
- Add a <code>README.md</code> with some build instructions for my co-authors and
- Perform a test compilation of the entire project.

## Using a project template

What if we can run a _single command_ to set up a new project? For this we can use <code>[cookiecutter](https://github.com/cookiecutter/cookiecutter)</code> {% cite cookiecutter %}. <code>cookiecutter</code> is a command-line utility that creates projects from _cookiecutters_ which are project templates.

Using a <code>cookiecutter</code> is easy. We can just run

{% highlight bash linenos %}
cookiecutter gl:martisak/latex-template
{% endhighlight %}

{% include figure.html url="/assets/images/latex_cookiecutter.gif" description="Running cookiecutter" colclass="col-md-10" %}

You will need a Gitlab account. Then you need to add these environment variables, for example by adding this to your `.zshrc`. [Read more on how to create a private Gitlab token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

{% highlight bash linenos %}
export GITLAB_API_PRIVATE_TOKEN=<your private token>
export GITLAB_API_USERNAME=<your username>
export GITLAB_URL=gitlab.com
{% endhighlight %}

Of course, some fields will always be the same (such as your name), so we can add our defaults to <code>~/.cookiecutterrc</code>.

{% highlight yaml linenos %}
default_context:
    full_name: "Martin Isaksson"
    email: "m@cyberdyne.se"
    gitlab_username: "martisak"
    affiliation: "Cyberdyne Systems"
    department: "AI lab"
    paper_title: "Lorem ipsum"
cookiecutters_dir: "~/.cookiecutters/"
{% endhighlight %}

After you have run this cookiecutter template, you will have a working 
directory that looks like this.

{% highlight text %}
├── Gemfile                         # For the test stage
├── Gemfile.lock                    # For the test stage
├── Makefile                        # For building
├── README.md                       # A stub README
├── chapters
│   ├── conclusion.tex              # A chapter in LaTeX
│   └── introduction.tex            # A chapter in LaTeX
├── figures
│   ├── Makefile                    # A Makefile for building the figures
│   ├── example.py                  # Python code for plotting
│   └── requirements.txt            # Requirements for the figure stage
├── lorem-ipsum.sublime-project     # A Sublime project file
├── lorem-ipsum.tex                 # This is the main LaTeX file
├── references
│   └── main.bib                    # References in bibtex
└── spec
    └── pdf_spec.rb                 # For the test stage
{% endhighlight %}

We see a lot of auxiliary files for testing and for setting up a Sublime project. If you have a better directory structure, feel free to fork my `cookiecutter` and make it better!

## Related work

There are [many existing cookiecutters](https://github.com/search?q=cookiecutter+latex&type=Repositories) for creating LaTeX documents. However, I didn't find one that fitted my needs, so I made my own.

[Yeoman](http://yeoman.io/) {% cite yeoman %} provides another generator ecosystem, is language agnostic and can be used to generate any kind of scaffolding. However, since it is Node.js-based it could be argued that it is more geared towards web development. It is very extensible and it is easy to create custom templates, assuming knowledge of [Node.js](https://nodejs.org).

An interesting feature of Yeoman generators is that it allows for sub-generators. These can be used for example to generate new chapters in our documents.

## Conclusion

So now we have a good starting point that you can use for your awesome next paper &mdash; all you need to do is fill in the blanks.

A remaining manual step is to find and copy the LaTeX template. If you always use the same template, this can be included in the cookiecutter template. In a future post we will look into how to use Pandoc and Markdown which will make choosing and switching templates easier.