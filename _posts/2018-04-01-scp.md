---
title: "Predicting strongest cell on secondary carrier using primary carrier data"
categories:
  - Publications
tags:
  - link
link: /publications/scp/
---

There is a rapid evolution in telecommunication with denser networks and systems operating on an increasing number of frequency bands. Also in the next generation 5G networks, even further densification is needed to be able to reach the tight requirements, implying more nodes on each carrier. Denser networks and more frequencies makes it challenging to ensure the best possible cell and frequency carrier assignment to a User Equipment (UE), without the UE needing to perform an excessive amount of inter-frequency measurements and reporting. In this paper, we propose a procedure of predicting the strongest cell of a secondary carrier, and the procedure is exemplified in a UE load-balancing use case. The prediction is based on only measurements on the primary carrier cells, avoiding costly inter-frequency measurements. Simulations of a realistic network deployment show that a UE selection based on the proposed secondary carrier prediction is significantly better than a random UE selection for load balancing.