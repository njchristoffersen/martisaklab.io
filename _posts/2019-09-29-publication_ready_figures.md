---
title:  Create publication ready figures with Matplotlib and TikZ
categories: reproducibility academia visualization
layout: "post"
tags:
  - visualization
  - matplotlib
  - tikz
  - pgf
  - plots
  - figures
  - example
image:
  path: /images/blog-2.jpg
description: Learn how to make your figures stand out with tikzplotlib and matplotlib.
---

**Figures in scientific papers often look less than professional, and
sometimes this can even get in the way of understanding the figure. In this
blog post we show how to use `matplotlib` and `tikzplotlib` to make
publication ready figures that look great and can be styled from the document
preamble. Beautiful and understandable figures can possibly lead to higher publication acceptance rate, at least I hope so...**

<!--more-->

## Introduction

For generating figures in my scientific papers I use
TikZ {% cite Tantau2007 %} but
until recently I have been using `matplotlib`&#160;{% cite Hunter2007 %} for
graphs and saved them as PDF.  When I found
[`tikzplotlib`](https://github.com/nschloe/tikzplotlib)&#160;{% cite SchlomerNicoa %} I found it to be awesome but needing a few tweaks. `tikzplotlib` is a Python tool for converting `matplotlib` figures into PGFPlots {% cite Feuersanger2014 %} figures for inclusion into LaTeX documents.

If there's one thing I loath (and you should too), it's _chartjunk_
{% cite Tufte:1986:VDQ:33404 %}. We should avoid _chartjunk_ at all cost.
_Chartjunk_ is everything in your figure that dilutes your message and adds
confusion {% cite Rougier2014 %}. Before `tikzplotlib`, I spent a lot of time
styling individual plots with `matplotlib`. With `tikzplotlib` we can put most
of that styling in the LaTeX document header.

{% include figure.html url="/images/pgfplot.png" description="Example figure produced with this method." %}

In this blog post we will explore `matplotlib` and `tikzplotlib`, remove some
_chartjunk_ and build a pipeline to include `matplotlib` images in LaTeX figures.

## Using `tikzplotlib`

### Making a plot

The first thing we need to do is to make a plot. We'll look at
the [simple plot](https://matplotlib.org/3.1.1/gallery/lines_bars_and_markers/simple_plot.html)
example from the `matplotlib` gallery {% cite hunter2019 %}. In my opinion this example has the
appearance of a `matplotlib` plot, and I'd like it to look a little more
professional.

{% include figure.html url="/images/simple_plot.png" description="Example figure from the Matplotlib gallery." %}

{% highlight python linenos %}
{% include pgfplots/figure.py %}
{% endhighlight %}

At the end we are importing `tikzplotlib` and using the `save` function to save a `tikz` file containing the plot.

For this example, we are using the following packages.

{% highlight python %}
{% include pgfplots/requirements.txt %}
{% endhighlight %}

Now we are ready to include this file in a document. Here we are using a TikZ
standalone-document as an example to include the `tikz` code in the document,
but some publishers rather want you to include a PDF-version.

{% highlight latex linenos %}
{% include pgfplots/figure.tex %}
{% endhighlight %}

We can compile this as a standalone figure into PNG and PDF by running
`pdflatex -shell-escape figure.tex`.

### Tweaking the output

If we look at the LaTeX code above, we see `pgplotset` with some options.
These options are used to set a fixed size of the plot area (excluding the
axes labels and the title), set the number of tickmarks etc. We are also
redefining the line width `semithick` to be as thick as `thick`.

The bounding box of figures produced in this way can be slightly different
depending on the labels and title and other things drawn outside the plot area.
This is usually not a problem, but will be painfully obvious if two figures 
are placed side-by-side. Therefore, we need to manually set the bounding box. 
We do this with a `sed` hack that inserts a `path` on line&#160;3 in the file `figure.pgf`.

{% highlight bash %}
sed -i -e '3i \\\path (-1.2,-1.1) rectangle (7.1cm, 3.6cm);' figure.pgf
{% endhighlight %}

This will add an invisible rectangle outside the figure, which will be used by
TikZ to determine the bounding box. It will be the
same for every figure we make, and in this way we can make sure that two
figures side by side will line up perfectly.

### Automation

To automate this build, we can use the following `Makefile`. The input files 
are `figure.py` and `figure.tex`, both of which are listed above.

{% highlight makefile linenos %}
{% include pgfplots/Makefile %}
{% endhighlight %}

We want to create the file `figure.png`. To do this we start with running
Python to generate the `.pgf` file that we then include in `figure.tex`.
Compiling `figure.tex` renders the figure and saves it as a `.png`. We get
a `.pdf` for free when using `pdflatex`.

### Including the figure in a document

We need to include the produced figure into a `figure` environment in our
document, and for this we can write the LaTeX code
below {% cite wikibooksfloats %}.

{% highlight tex linenos %}
\begin{figure}
    \centering
    \input{figure.pgf}
    \caption{Example figure produced with this method.}
\end{figure}
{% endhighlight %}

If your publisher requires you to submit a manuscript with PDF figures, you can
use the standalone document above and use the standard `\includegraphics`
command instead of the `\input` command.

## Related Work

{% cite Rougier2014 %} listed ten simple rules for better figures, of which
rule number 5 was _Do not trust the default_ and number 8 was related to
avoiding _chartjunk_. It also listed a few good tools, among others
`matplotlib`, TikZ and PGF, but didn't tell us how to use them.

The entire library of work by Edward Tufte is hugely inspirational to us.
{% cite Tufte:1986:VDQ:33404 %} tells us not to put too much ink on the paper and
gives us examples on how to achieve this.

{% cite Vandenbroucke2019 %} teaches us how to use TikZ for non-plots.
Besides being a good introduction to TikZ we also learn how to compile
standalone pictures and how to embed the TikZ code into a LaTeX figure.

`matplotlib`&#160;{% cite Hunter2007 %} has support for 
[style sheets](https://matplotlib.org/stable/gallery/style_sheets/index.html) 
where you can use existing styles or create 
your own styles. This can make your figures look much nicer, quite easily. In 
this way we delegate styling to `matplotlib` as opposed to 
TikZ&#160;{% cite Tantau2007 %} in this work.

## Conclusion

We have looked at how to make `matplotlib` figures look more professional by
using `tikzplotlib` and some tweaks. The `Makefile` we created should go into
the `figures` directory of your manuscript so that you can use `make -C figures
all` as a dependency to your normal `make report` target.

I find it easy to control the style of my plots from the preamble of my
manuscript and just rebuild the plots if the content changed.

In my humble opinion, easily understandable figures that makes it easy for a
reviewer to understand the message that your are trying to send, should make
it harder for the reviewer to reject your paper. In fact there is some
evidence&#160;{% cite Huang2018 %} that the visual appearance of a paper is
important and that improving the paper gestalt reduces risk of getting 
rejected.