---
title: Secure Federated Learning in 5G Mobile Networks
author: M. Isaksson, K. Norrman
in: IEEE GLOBECOM 2020
tags:
    - 5G
    - federated learning
    - machine learning
    - security
    - privacy
date: 2020-04-14
online: https://arxiv.org/
image:
  path: /images/blog-ai.jpg
online: https://arxiv.org/abs/2004.06700
bibtex: |
    @INPROCEEDINGS{9322479,
      author    = {Martin Isaksson and
                   Karl Norrman},
      title     = {{Secure Federated Learning in 5G Mobile Networks}},
      booktitle = {GLOBECOM 2020 - 2020 IEEE Global Communications Conference},
      year      = {2020},
      url       = {https://ieeexplore.ieee.org/document/9322479},
      month     = {December},
      pages     = {1-6},
      doi       = {10.1109/GLOBECOM42002.2020.9322479}}
    }
description: A secure integration of Federated Learning in the 3GPP 5G Network Data Analytics framework.
abstract: | 
    Machine Learning (ML) is an important enabler for optimizing, securing and managing mobile networks. This leads to increased collection and processing of data from network functions, which in turn may increase threats to sensitive end-user information. Consequently, mechanisms to reduce threats to end-user privacy are needed to take full advantage of ML. We seamlessly integrate Federated Learning (FL) into the 3GPP 5G Network Data Analytics (NWDA) architecture, and add a Multi-Party Computation (MPC) protocol for protecting the confidentiality of local updates. We evaluate the protocol and find that it has much lower overhead than previous work, without affecting ML performance.
---

<div class="container">
<div class="row">
<div class="col-md-8">
{{ page.abstract }}
</div>
<div class="col-md-4">
<div><img src="/images/kth_poster_challenge.png" alt="Poster" /></div>
<div class="caption">Secure Federated Learning in 5G Mobile Networks poster.</div>
</div>
</div>
</div>

<h2>Background</h2>

This is a video for a technical report done for the 2020 WASP Project Course.

{% include responsive-embed url="https://www.youtube.com/embed/JmuZcVAtLKY" ratio="16:9" %}
