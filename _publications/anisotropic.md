---
title: Vehicle Detection using Anisotropic Magnetoresistors
author: Isaksson, Martin
in: Exjobb - Institutionen för signaler och system, Chalmers tekniska högskola
date: 2008-04-01
online: http://studentarbeten.chalmers.se/publication/70865-vehicle-detection-using-anisotropic-magnetoresistors
bibtex: |
    @mastersthesis{Isaksson2008,
        author = {Isaksson, Martin},
        publisher = {Institutionen f{\"{o}}r signaler och system, Chalmers tekniska h{\"{o}}gskola},
        school = {Chalmers tekniska h{\"{o}}gskola},
        series = {Ex - Institutionen f{\"{o}}r signaler och system, Chalmers tekniska h{\"{o}}gskola, no: EX034/2008},
        title = {{Vehicle detection using anisotropic magnetoresistors}},
        url = {https://hdl.handle.net/20.500.12380/70865},
        year = {2008}
    }
image: 
    path: /images/traffic.jpg
abstract: |
    A Wireless Sensor Network (WSN) of anisotropic magnetoresistor sensors offers a low-cost alter- native to other traffic measurement technologies. The WSNs offer better reliability than other solutions, they offer more information, can be deployed quickly and be reused. In this thesis the sensor algorithms used for detection, velocity estimation, queue detection and classification in such a network are evaluated based on simulated and measured data. A number of algorithms are evaluated and the results are compared. A new algorithm for speed estimation using two sensor nodes is proposed and evaluated. It is found to be much better than earlier algorithms, requiring a signal to noise ratio (SNR) of 20 dB less than the traditional algorithm.
---

{{ page.abstract }}

The [LaTeX code is available on Github](https://github.com/martisak/anisotropic).