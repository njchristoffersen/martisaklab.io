---
title: Predicting strongest cell on secondary carrier using primary carrier data
author: H. Rydén, J. Berglund, M. Isaksson, F. Gunnarsson, R. Cöster.
in: 2018 IEEE Wireless Communications and Networking Conference Workshops (WCNCW)
tags: 
    - 5G mobile communication
    - next generation networks
    - telecommunication traffic
    - primary carrier data
    - frequency bands
    - frequency carrier assignment
    - User Equipment
    - UE load-balancing use case
    - primary carrier cells
    - secondary carrier prediction
    - random UE selection
    - next generation 5G networks
    - inter-frequency measurements
    - Conferences;Frequency measurement
    - Load modeling
    - Timing
    - Urban areas
    - Self-organizing networks
    - Load management
date: 2018-04-01
online: https://ieeexplore.ieee.org/abstract/document/8369000/
bibtex: |
    @INPROCEEDINGS{8369000, 
        author={Ryd\'en, Henrik and Berglund, Joel and Isaksson, Martin and C\"oster, Rickard and Gunnarsson, Fredrik}, 
        booktitle={2018 IEEE Wireless Communications and Networking Conference Workshops (WCNCW)}, 
        title={Predicting strongest cell on secondary carrier using primary carrier data}, 
        year={2018}, 
        volume={}, 
        number={}, 
        pages={137-142}, 
        keywords={5G mobile communication;next generation networks;telecommunication traffic;primary carrier data;frequency bands;frequency carrier assignment;User Equipment;UE load-balancing use case;primary carrier cells;secondary carrier prediction;random UE selection;next generation 5G networks;inter-frequency measurements;Conferences;Frequency measurement;Load modeling;Timing;Urban areas;Self-organizing networks;Load management}, 
        doi={10.1109/WCNCW.2018.8369000}, 
        ISSN={}, 
        month={April},
        url = {https://doi.org/10.1109/wcncw.2018.8369000},
        isbn = {9781538611548}
    }
abstract: |
    There is a rapid evolution in telecommunication with denser networks and systems operating on an increasing number of frequency bands. Also in the next generation 5G networks, even further densification is needed to be able to reach the tight requirements, implying more nodes on each carrier. Denser networks and more frequencies makes it challenging to ensure the best possible cell and frequency carrier assignment to a User Equipment (UE), without the UE needing to perform an excessive amount of inter-frequency measurements and reporting. In this paper, we propose a procedure of predicting the strongest cell of a secondary carrier, and the procedure is exemplified in a UE load-balancing use case. The prediction is based on only measurements on the primary carrier cells, avoiding costly inter-frequency measurements. Simulations of a realistic network deployment show that a UE selection based on the proposed secondary carrier prediction is significantly better than a random UE selection for load balancing.
---

{{ page.abstract }}

